// Advent of Code 2020
// Asbjørn Olling

use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;

fn read_input() -> io::Result<Vec<i32>> {
    // open file
    let path = Path::new("input.txt");

    let mut file = File::open(&path)?;

    // read file to string
    let mut file_str = String::new();
    file.read_to_string(&mut file_str)?;

    // split into lines
    let lines = file_str.trim().split("\n");

    // parse as numbers, sort immediately
    let numbers: Vec<i32> = lines.map(|l| l.parse::<i32>().unwrap()).collect();

    Ok(numbers)
}

fn main() -> io::Result<()> {
    // read input and sort it
    let mut input = read_input()?;
    input.sort();

    // go through numbers
    for num in &input {
        for othernum in &input {
            // pattern match on addition
            match num + othernum {
                x if x > 2020 => break,
                2020 => {
                    println!("{:?}", num * othernum);
                    return Ok(());
                }
                _ => continue,
            };
        }
    }
    panic!("Did not find answer");
}
