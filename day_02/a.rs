use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use regex::Regex;

#[derive(Debug)]
struct Password {
    min: i32,
    max: i32,
    c: char,
    password: String,
}

fn read_input() -> io::Result<Vec<Password>> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    // match lines on regex, build Password structs from capture groups
    let re = Regex::new(r"(?m)^([0-9]+)-([0-9]+) (.): (.+)$").unwrap();
    let passwords: Vec<Password> = re.captures_iter(&text).map(
        |cap| {
            let min = cap[1].parse::<i32>().unwrap();
            let max = cap[2].parse::<i32>().unwrap();
            let c = cap[3].parse::<char>().unwrap();
            let password = cap[4].to_string();
            Password { min, max, c, password }
        }
    ).collect();

    Ok(passwords)
}

fn main() -> io::Result<()> {
    let entries = read_input()?;

    let mut total = 0;
    for entry in entries {
        let count = entry.password.matches(entry.c).count() as i32;
        if entry.min <= count && count <= entry.max {
            total += 1;
        }
    }

    println!("{}", total);
    Ok(())
}
