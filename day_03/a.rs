use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;

fn read_input() -> io::Result<Vec<Vec<bool>>> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    let forest: Vec<Vec<bool>> = text.trim().split("\n").map(
        |line| line.chars().map(
            |c| c == '#'
        ).collect::<Vec<bool>>()
    ).collect();

    Ok(forest)
}

fn main() -> io::Result<()> {
    let forest = read_input()?;

    let width = forest[0].len();
    let height = forest.len();
    let step_x = 3;
    let step_y = 1;
    let range = 0..(height / step_y);
    let path: Vec<bool> = range.map(|i| forest[i*step_y][(i*step_x)%width]).collect();

    let tree_count = path.iter().filter(|t| *t == &true).count();

    // println!("{:?}", path);
    println!("{:?}", tree_count);
    Ok(())
}
