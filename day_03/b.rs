use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;

fn read_input() -> io::Result<Vec<Vec<bool>>> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    let forest: Vec<Vec<bool>> = text
        .trim()
        .split("\n")
        .map(|line| line.chars().map(|c| c == '#').collect::<Vec<bool>>())
        .collect();

    Ok(forest)
}

fn trees_on_path(forest: &Vec<Vec<bool>>, step_x: usize, step_y: usize) -> usize {
    // figure out some numbers
    let height = forest.len();
    let width = forest[0].len();
    let range = 0..(height / step_y);

    // do the path
    let path: Vec<bool> = range
        .map(|i| forest[i * step_y][(i * step_x) % width])
        .collect();

    // count the trees on the path
    path.iter().filter(|t| **t).count()
}

const SLOPES: [(usize, usize); 5] = [
    (1, 1),
    (3, 1),
    (5, 1),
    (7, 1),
    (1, 2)
];

fn main() -> io::Result<()> {
    let forest = read_input()?;
    let tree_counts = SLOPES.iter().map(
        |(x, y)| trees_on_path(&forest, *x, *y)
    );
    let result = tree_counts.fold(1, |c, total| c * total);
    println!("{:?}", result);
    Ok(())
}
