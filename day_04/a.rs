use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use std::collections::HashMap;

fn read_input() -> io::Result<Vec<HashMap<String, String>>> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    // split by double newline, make each entry into a hashmap
    let passports: Vec<HashMap<String, String>> = text.split("\n\n").map(
        |line| {
            let mut passport = HashMap::new(); 
            // replace newlines with spaces
            for field in line.trim().replace("\n", " ").split(" ") {
                // split on ":", make hashmap entry
                let split = field.split(":").collect::<Vec<&str>>();
                passport.insert(split[0].to_string(), split[1].to_string());
            }
            passport
        }
    ).collect();

    Ok(passports)
}

fn passport_valid(passport: &HashMap<String, String>) -> bool {
    passport.contains_key("byr")
        && passport.contains_key("iyr")
        && passport.contains_key("eyr")
        && passport.contains_key("hgt")
        && passport.contains_key("hcl")
        && passport.contains_key("ecl")
        && passport.contains_key("pid")
}

fn main() -> io::Result<()> {
    let passports = read_input()?;

    let mut count = 0;
    for passport in &passports {
        if passport_valid(&passport) {
            count += 1;
        }
    }

    println!("{:?}", count);
    Ok(())
}
