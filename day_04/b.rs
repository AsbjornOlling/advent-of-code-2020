use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use std::collections::HashMap;
use regex::Regex;

fn read_input() -> io::Result<Vec<HashMap<String, String>>> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    // split by double newline, make each entry into a hashmap
    let passports: Vec<HashMap<String, String>> = text.split("\n\n").map(
        |line| {
            let mut passport = HashMap::new();
            // replace newlines with spaces
            for field in line.trim().replace("\n", " ").split(" ") {
                // split on ":", make hashmap entry
                let split = field.split(":").collect::<Vec<&str>>();
                passport.insert(split[0].to_string(), split[1].to_string());
            }
            passport
        }
    ).collect();

    Ok(passports)
}

fn passport_valid(passport: &HashMap<String, String>) -> bool {
    let byr_ok: bool = match passport.get("byr") {
        None => false,
        Some(byr) => match byr.parse::<i32>() {
            Err(_) => false,
            Ok(i) => if 2002 < i || i < 1920 { false } else { true }
        }
    };

    let iyr_ok = match passport.get("iyr") {
        None => false,
        Some(iyr) => match iyr.parse::<i32>() {
            Err(_) => false,
            Ok(i) => if 2020 < i || i < 2010 { false } else { true }
        }
    };

    let eyr_ok = match passport.get("eyr") {
        None => false,
        Some(eyr) => match eyr.parse::<i32>() {
            Err(_) => false,
            Ok(i) => if 2030 < i || i < 2020 { false } else { true }
        }
    };

    let height_re = Regex::new(r"^(\d{2}|\d{3})(in|cm)$").unwrap();
    let hgt_ok: bool = match passport.get("hgt") {
        None => false,
        Some(hgt) => match height_re.captures(hgt) {
            None => false,
            Some(caps) => match caps[1].parse::<i32>() {
                Err(_) => false,
                Ok(height) => match &caps[2] {
                    "in" => if height < 59 || 76 < height { false } else { true },
                    "cm" => if height < 150 || 193 < height { false } else { true },
                    _ => false
                }
            }
        }
    };

    let color_re = Regex::new(r"^#[0-9a-f]{6}$").unwrap();
    let hcl_ok: bool = match passport.get("hcl") {
        None => false,
        Some(hcl) => match color_re.is_match(hcl) {
            false => false,
            true => true
        }
    };

    let ecls = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
    let ecl_ok: bool = match passport.get("ecl") {
        None => false,
        Some(ecl) => if ecls.contains(&ecl.as_str()) { true } else { false }
    };

    let numbers_re = Regex::new(r"^\d{9}$").unwrap();
    let pid_ok = match passport.get("pid") {
        None => false,
        Some(pid) => if numbers_re.is_match(pid) { true } else { false }
    };

    // println!("{:?}", [byr_ok, iyr_ok, eyr_ok, hgt_ok, hcl_ok, ecl_ok, pid_ok]);
    return byr_ok && iyr_ok && eyr_ok && hgt_ok && hcl_ok && ecl_ok && pid_ok;
}

fn main() -> io::Result<()> {
    let passports = read_input()?;
    println!("{:?}", passports);

    let count = passports.iter().filter(
        |p| passport_valid(p)
    ).count();

    println!("{:?}", count);
    Ok(())
}
