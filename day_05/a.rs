use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use regex::Regex;

#[derive(Debug)]
struct Seat {
    row: i32,
    col: i32,
}

fn read_input() -> io::Result<Vec<Seat>> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    let re = Regex::new(r"(?m)^([FB]{7})([LR]{3})$").unwrap();
    let seats: Vec<Seat> = re.captures_iter(&text).map(
        |cap| {
            let rowstr: &str = &cap[1].replace("F", "0").replace("B", "1");
            let colstr: &str = &cap[2].replace("L", "0").replace("R", "1");
            Seat {
                row: isize::from_str_radix(rowstr, 2).unwrap() as i32,
                col: isize::from_str_radix(colstr, 2).unwrap() as i32,
            }
        }
    ).collect();

    Ok(seats)
}

fn main() -> io::Result<()> {
    let seats = read_input()?;

    let seat_ids = seats.iter().map(
        |s| (s.row * 8) + s.col
    ).collect::<Vec<i32>>();

    let max = seat_ids.iter().max();

    println!("{:?}", max);
    Ok(())
}
