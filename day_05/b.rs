use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use regex::Regex;

#[derive(Debug)]
struct Seat {
    row: i32,
    col: i32,
}

fn read_input() -> io::Result<Vec<Seat>> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    let re = Regex::new(r"(?m)^([FB]{7})([LR]{3})$").unwrap();
    let seats: Vec<Seat> = re.captures_iter(&text).map(
        |cap| {
            let rowstr: &str = &cap[1].replace("F", "0").replace("B", "1");
            let colstr: &str = &cap[2].replace("L", "0").replace("R", "1");
            Seat {
                row: isize::from_str_radix(rowstr, 2).unwrap() as i32,
                col: isize::from_str_radix(colstr, 2).unwrap() as i32,
            }
        }
    ).collect();

    Ok(seats)
}

fn seat_id(s: &Seat) -> i32 {
    (s.row * 8) + s.col
}

fn main() -> io::Result<()> {
    let seats = read_input()?;

    let mut seat_ids: Vec<(&Seat, i32)> = seats.iter().map(
        |s| (s, seat_id(s))
    ).collect();
    seat_ids.sort_by(|(_, a), (_, b)| a.cmp(b));

    for i in 1..seat_ids.len()-1 {
        let (_, before_sid) = seat_ids[i-1];
        let (_, this_sid) = seat_ids[i];
        let (_, after_sid) = seat_ids[i+1];
        if !(before_sid == this_sid-1 && after_sid == this_sid+1) {
            println!("{:?}", this_sid);
        }
    }

    // println!("{:#?}", seat_ids);
    Ok(())
}
