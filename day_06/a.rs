use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;

fn read_input() -> io::Result<Vec<Vec<char>>> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    let groups: Vec<Vec<char>> = text.split("\n\n").map(
        |groupstr| {
            let mut qs: Vec<char> = groupstr.trim().replace("\n", "").chars().collect();
            qs.sort();
            qs.dedup();
            qs
        }
    ).collect();

    Ok(groups)
}

fn main() -> io::Result<()> {
    let groups = read_input()?;

    let counts: usize = groups.iter().map(
        |group| group.len()
    ).sum();

    println!("{:#?}", counts);
    Ok(())
}
