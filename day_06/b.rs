use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;

fn read_input() -> io::Result<Vec<Vec<Vec<char>>>> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    let groups: Vec<Vec<Vec<char>>> = text.trim().split("\n\n").map(
        |groupstr| {
            let person_strs: Vec<String> = groupstr.split("\n").map(
                |s| s.to_owned()
            ).collect::<Vec<String>>();

            let person_chars: Vec<Vec<char>> = person_strs.iter().map(
                |s| s.chars().collect::<Vec<char>>()
            ).collect();
            person_chars
        }
    ).collect();

    Ok(groups)
}

fn questions_in_group(group: &Vec<Vec<char>>) -> Vec<char> {
    let mut answers = group.iter().fold(
        vec![],
        |mut acc, x| {
            acc.extend(x);
            acc
        }
    );
    answers.sort();
    answers.dedup();
    answers
}

fn main() -> io::Result<()> {
    let groups = read_input()?;

    let counts: Vec<i32> = groups.iter().map(
        |group| {
            let questions = questions_in_group(&group);
            let questions_answered = questions.iter().filter(
                |question| {
                    // count persons in group that answered `question`
                    let persons_answered = group.iter().filter(
                        |person| person.contains(&question)
                    ).count();
                    // true if everybody answered the question
                    persons_answered == group.len()
                }
            ).count();
            questions_answered as i32
        }
    ).collect();

    let answer: i32 = counts.iter().sum();

    println!("{:#?}", answer);
    Ok(())
}
