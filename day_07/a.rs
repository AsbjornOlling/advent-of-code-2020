use regex::Regex;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;

#[derive(Debug, Hash, Eq, PartialEq)]
struct Bag {
    texture: String,
    color: String,
}

#[derive(Debug)]
struct BagRelation {
    bag: Bag,
    contain: HashMap<Bag, i32>,
}

fn read_input() -> io::Result<Vec<BagRelation>> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    // go for it
    let re = Regex::new(r"(?m)^(?P<texture>[a-z]+) (?P<color>[a-z]+) bags contain ((?P<others>(\d+ [a-z]+ [a-z]+ bags?(, |\.))+)|(?P<none>no other bags\.))$").unwrap();
    let other_re = Regex::new(r"(?P<num>\d+) (?P<texture>[a-z]+) (?P<color>[a-z]+) bag").unwrap();
    let bags: Vec<BagRelation> = re.captures_iter(&text).map(
        |cap| {
            let thisbag = Bag {
                texture: cap["texture"].to_string(),
                color: cap["color"].to_string()
            };
            let contained: HashMap<Bag, i32> = match cap.name("others") {
                None => HashMap::new(),
                Some(others) => {
                    other_re.captures_iter(others.as_str()).fold(
                        HashMap::new(),
                        |mut map, cap2| {
                            let bag = Bag {
                                texture: cap2["texture"].to_string(),
                                color: cap2["color"].to_string()
                            };
                            map.insert(
                                bag,
                                cap2["num"].parse::<i32>().unwrap()
                            );
                            map
                        }
                    )
                }
            };
            BagRelation {
                bag: thisbag,
                contain: contained
            }
        }
    ).collect();
    
    Ok(bags)
}

fn parent_relations(rels: &Vec<BagRelation>) -> HashMap<&Bag, Vec<&Bag>> {
    // make hashmap of <bag, bags that contain it>, for all bags
    let mut parent_bags: HashMap<&Bag, Vec<&Bag>> = HashMap::new();
    for rel in rels {
        let parent: &Bag = &rel.bag;
        for contained_bag in rel.contain.keys() {
            match parent_bags.get(contained_bag) {
                None => {
                    parent_bags.insert(contained_bag, vec![parent]);
                },
                Some(existing) => {
                    let mut new = existing.to_vec();
                    new.push(parent);
                    parent_bags.insert(contained_bag, new);
                }
            } 
        }
    }
    parent_bags
}

fn all_parents<'a>(b: &'a Bag, parents: &'a HashMap<&Bag, Vec<&Bag>>) -> HashSet<&'a Bag> {
    // recursively find all parents of bag
    let mut hs = match parents.get(b) {
        None => HashSet::new(),
        Some(bs_parents) => {
            let val = bs_parents.iter().map(
                |p| all_parents(p, parents)
            ).flatten().collect();
            val
        }
    };
    hs.insert(b);
    hs
}


fn main() -> io::Result<()> {
    let mybag: Bag = Bag {
        texture: "shiny".to_string(),
        color: "gold".to_string()
    };

    let input = read_input()?;
    let parents = parent_relations(&input);

    let my_parents = all_parents(&mybag, &parents);

    // println!("{:#?}", input);
    // println!("{:#?}", parents);
    println!("{:#?}", my_parents.len() - 1);
    Ok(())
}
