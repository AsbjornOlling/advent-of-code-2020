use regex::Regex;
use std::collections::HashMap;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;

#[derive(Debug, Hash, Eq, PartialEq)]
struct Bag {
    texture: String,
    color: String,
}

type Relation = HashMap<Bag, HashMap<Bag, i32>>;

fn read_input() -> io::Result<Relation> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    // go for it
    let re = Regex::new(r"(?m)^(?P<texture>[a-z]+) (?P<color>[a-z]+) bags contain ((?P<others>(\d+ [a-z]+ [a-z]+ bags?(, |\.))+)|(?P<none>no other bags\.))$").unwrap();
    let other_re = Regex::new(r"(?P<num>\d+) (?P<texture>[a-z]+) (?P<color>[a-z]+) bag").unwrap();
    let bags: Relation =
        re.captures_iter(&text)
            .fold(HashMap::new(), |mut rels, cap| {
                let thisbag = Bag {
                    texture: cap["texture"].to_string(),
                    color: cap["color"].to_string(),
                };
                let contained: HashMap<Bag, i32> = match cap.name("others") {
                    None => HashMap::new(),
                    Some(others) => other_re.captures_iter(others.as_str()).fold(
                        HashMap::new(),
                        |mut map, cap2| {
                            let bag = Bag {
                                texture: cap2["texture"].to_string(),
                                color: cap2["color"].to_string(),
                            };
                            map.insert(bag, cap2["num"].parse::<i32>().unwrap());
                            map
                        },
                    ),
                };
                rels.insert(thisbag, contained);
                rels
            });
    Ok(bags)
}

fn count_children(b: &Bag, rels: &Relation) -> i32 {
    // find this node's children
    match rels.get(b) {
        None => 0,
        Some(children) => {
            let csum: i32 = children
            .iter()
            .map(|(child_bag, child_count)| child_count * count_children(child_bag, rels))
            .sum();
            // println!("{:?}", csum);
            csum + 1
        },
    }
}

fn main() -> io::Result<()> {
    let mybag: Bag = Bag {
        texture: "shiny".to_string(),
        color: "gold".to_string(),
    };

    let input = read_input()?;
    let child_count = count_children(&mybag, &input);
    println!("{:#?}", child_count - 1);
    Ok(())
}
