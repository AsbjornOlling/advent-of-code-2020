use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::path::Path;
use regex::Regex;

#[derive(Debug, Clone)]
struct State {
    instructions: Vec<(String, i32)>,
    current: usize,
    acc: i32,
    count: Vec<i32>,
}

fn read_input() -> io::Result<State> {
    // open file
    let path = Path::new("input.txt");
    let mut file = File::open(&path)?;

    // read file to string
    let mut text = String::new();
    file.read_to_string(&mut text)?;

    let re = Regex::new(r"(?m)^([a-z]{3}) \+?(-?\d+)$").unwrap();
    let instructions: Vec<(String, i32)> = re
        .captures_iter(&text)
        .map(|cap| (cap[1].to_string(), cap[2].parse::<i32>().unwrap()))
        .collect();

    let len = instructions.len();
    let state = State {
        instructions: instructions,
        current: 0,
        acc: 0,
        count: vec![0; len]
    };

    Ok(state)
}

fn step(state: &State) -> State {
    let mut new_state = state.clone();

    // find current instruction
    let (code, arg): &(String, i32) = &state.instructions[state.current];

    // count the execution of this line
    new_state.count[state.current] += 1;

    // increment program counter

    // execute opcode
    match &code[..] {
        "nop" => {
            new_state.current += 1;
            new_state
        },
        "acc" => {
            new_state.current += 1;
            new_state.acc += arg;
            new_state
        },
        "jmp" => {
            // println!("curr {:?} arg {:?}", new_state.current, arg);
            new_state.current = (new_state.current as i32 + arg) as usize;
            new_state
        },
        _ => {
            panic!("Unsupported opcode")
        }
    }
}

fn possible_corruptions(state: &State) -> Vec<State> {
    // produce all possible replacements of 'jmp' with 'nop' and vice versa
    state.instructions.iter().enumerate().fold(
        vec![],
        |mut result, (i, (code, arg))| {
            // find new instruction
            let new_opcode: Option<&str> = match &code[..] {
                "jmp" => Some("nop"),
                "nop" => Some("jmp"),
                _ => None
            };
            
            // if any new instruction, make the program copy
            // and add it to the accumulator
            match new_opcode {
                Some(opcode) => {
                    let mut new_state = state.clone();
                    new_state.instructions[i] = (opcode.to_string(), *arg);
                    result.push(new_state);
                    result
                },
                // if nothing new, return without changes
                None => result
            }
        }
    )
}

fn is_done(state: &State) -> bool {
    state.current == state.instructions.len()
}

fn main() -> io::Result<()> {
    let mut state = read_input()?;
    let mut corruptions = possible_corruptions(&state);
    println!("{:?}", corruptions.len());

    loop {
        corruptions = corruptions.iter().map(step).collect();
        for completed in corruptions.iter().filter(|x| is_done(x)) {
            println!("DONE: {:?}", completed.acc);
        }
    }

    Ok(())
}
